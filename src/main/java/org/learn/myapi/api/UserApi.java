package org.learn.myapi.api;

import java.util.List;

import org.learn.myapi.dto.UserDto;
import org.learn.myapi.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserApi {

    private final UserService service;

    @RequestMapping(method=RequestMethod.GET)
    public ResponseEntity<List<UserDto>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @RequestMapping(method=RequestMethod.GET, path="/{id}")
    public ResponseEntity<UserDto> findOne (
        @PathVariable final int id
    ) {
        UserDto user = service.findOne(id);
        return ResponseEntity.ok(user);
    }
}
