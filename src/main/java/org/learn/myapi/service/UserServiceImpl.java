package org.learn.myapi.service;

import java.util.List;
import java.util.Optional;

import org.learn.myapi.dao.UserRepository;
import org.learn.myapi.domain.User;
import org.learn.myapi.dto.UserDto;
import org.learn.myapi.service.converter.UserConverter;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final UserConverter converter;

    public List<UserDto> findAll() {
        List<User> users = repository.findAll();
        return users
            .stream()
            .map(domain -> this.converter.convertTo(domain))
            .toList();
    }

    public UserDto findOne (final int id) {
        Optional<User> user = repository.findById(id);
        return user
            .map(domain -> this.converter.convertTo(domain))
            .orElse(null);
    }
}
