package org.learn.myapi.service;

import java.util.List;

import org.learn.myapi.dto.UserDto;

public interface UserService {
    List<UserDto> findAll();

    UserDto findOne(final int id);
}
