package org.learn.myapi.service.converter;

import org.learn.myapi.domain.User;
import org.learn.myapi.dto.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {
    public UserDto convertTo(User domain) {
        UserDto dto = new UserDto();

        dto.setId(domain.getId());
        dto.setName(domain.getName());

        return dto;
    }
}
